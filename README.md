auto-foia
=========

Automatically send out templated Freedom of Information requests.

It's currently hardcoded to what I currently am sending out, so you'll
need to hack it a bit.

config.json specifies what templates should be sent, and to whom. Right
now the scheduling system only allows for monthly requests, though it
should be straightforward to add yearly or longer time periods.

In California there's not much value for time periods shorter than a month
since your request can take up to 10 + 30 business days to process anyways.

All code released under the GPL, v3 or any later version. See COPYING for
more details.

Discussion can take place on the [sb1421@lists.riseup.net](https://lists.riseup.net/www/info/sb1421)
mailing list.
