"""
Copyright (C) 2019 Kunal Mehta <legoktm@riseup.net>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
from email.message import EmailMessage
import json
import os
import smtplib

__dir__ = os.path.dirname(__file__)
ARCHIVES = os.path.join(__dir__, "archives")
TODAY = datetime.date.today()
TEMPLATES = os.path.join(__dir__, "templates")


def template(name, kwargs):
    with open(os.path.join(TEMPLATES, name)) as f:
        text = f.read()
    return text.format(**kwargs)


def main():
    with open(os.path.join(__dir__, "config.json")) as f:
        conf = json.load(f)
    gconf = conf.pop("global")
    for name, subconf in conf.items():
        process(subconf, gconf)


def is_scheduled(schedule):
    if schedule != "monthly":
        raise NotImplementedError
    return TODAY.day == 1


def calculate_period(schedule):
    if schedule != "monthly":
        raise NotImplementedError
    delta = datetime.timedelta(days=1)
    # Today is the first, so period ended yesterday
    end = TODAY - delta
    # First of the month
    start = datetime.date(end.year, end.month, 1)
    period = end.strftime("%B %Y")
    pfname = end.strftime("%Y-%m")
    return [start, end, period, pfname]


def generate_to(to):
    if isinstance(to, str):
        return to
    return ", ".join(to)


def send_message(msg):
    with open(os.path.join(__dir__, "smtp.json")) as f:
        sconf = json.load(f)
    with smtplib.SMTP(sconf["host"]) as server:
        # Apparently Gmail requires these.
        server.ehlo()
        server.starttls()
        server.login(sconf["user"], sconf["password"])
        server.send_message(msg)


def build_archive_key(*args):
    return (
        "-".join(args).replace("@", "_").replace(".", "_").replace(" ", "_")
        + ".msg"
    )


def process(subconf, gconf):
    if not is_scheduled(subconf["schedule"]):
        print("%s is not scheduled today." % subconf["template"])
        return False
    fmt = "%B %d, %Y"
    start, end, period, pfname = calculate_period(subconf["schedule"])
    params = {"start": start.strftime(fmt), "end": end.strftime(fmt)}
    for target in subconf["targets"]:
        to = generate_to(target["to"])
        archive_key = build_archive_key(subconf["template"], pfname, to)
        archive_path = os.path.join(ARCHIVES, archive_key)
        if os.path.exists(archive_path):
            print("Already sent %s, skipping." % archive_key)
            continue
        t_params = dict(**params, **target["params"])
        print(t_params)
        msg = EmailMessage()
        msg.set_content(template(subconf["template"], t_params))
        msg["Subject"] = subconf["subject"].format(period=period)
        msg["From"] = gconf["from"]
        msg["To"] = to
        print("Sending message...")
        send_message(msg)
        print("Sent.")
        # Create an archive so we don't double send...
        with open(archive_path, "wb") as f:
            f.write(bytes(msg))
        print("Archived.")


if __name__ == "__main__":
    main()
